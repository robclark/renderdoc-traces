renderdoc-traces
================

This is a repository of redistributable renderdoc traces, and tools
for Mesa driver development using them.

When capturing traces for inclusion in the repository, please ensure
that you have accurate licensing information included.

If you're trying to capture traces of Android applications, you will
need an unlocked Android device, and I highly recommend using Baldur's
binary builds from the main renderdoc repository instead of trying to
build the Android side yourself.

run.py
======

This is the main tool currently, using two shell scripts to control
the driver being loaded and running a set of traces with each one,
capturing FPS values  (based on time between glFinish() at start and
end of frame) for a set of frames and doing stats on the results.

Requires an installation of renderdoc (specifically renderdoccmd) in
your PATH.  If you're not using the shim below, you need to have
renderdoc built from https://github.com/anholt/renderdoc/tree/fps-hacks

Why glFinish()?  Because we want to capture both GPU and CPU
overheads.  CPU overhead won't match the real application due to
running from a trace instead of the original app, but it would be bad
to ignore it in performance testing entirely.

renderdoc\_replay\_shim.so
==========================

Built using the meson build infrastructure, this LD\_PRELOAD can be used
with the --preload argument to replay traces using upstream renderdoc.
On some systems our preload shim and renderdoc's interposer may interfere.

Limitations
===========

* On each frame drawn, renderdoccmd replay sets up the initial GL
  state again.  This will include compiling programs.

* I'm not sure if the section we're capturing with glFinish() includes
  the extra checkerboard or not.  I think it's not, but drawing that
  checkerboard is still a waste for this tool.

* Window system interactions are totally ignored.  The replay happens
  to a texture, which only approximates what the window system's
  surface would have looked like.

* group_size != 1 may overstate our confidence in results.  We sample multiple
  frames of replay per renderdoccmd run, do reduce the overhead of loading up
  renderdoc.  If there is noise in the perf of your system that extends over a
  handful of frames (performance governors, thermal throttling), a
  group_size of 5 may end up being effectively a single sample of "you were
  throttled" rather than 5 independent samples, invalidating the assumptions of
  the statistics.  As with compare_perf, the solution usually is to do
  --frames 0 indefinite testing while you go to lunch so that you take an
  excessive number of samples.  Be wary of small sample counts.